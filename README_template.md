# Software Studio 2021 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Filled Rectangle                                 |           | Y         |
| Filled Circle                                    |           | Y         |


---

### How to use 
從左到右的工具分別是 畫筆 > 橡皮擦 > 長方形 > 圓形 > 三角形 > 下載 > 下一步 > 上一步 > 輸入文字 > 調整字體大小 > 調整字型 > 重製化布 > 選取顏色 > 選取畫筆、形狀粗細大小 > 上傳檔案，下面一行分別是實心的長方形跟實心的圓型

預設的功能是畫筆，可以馬上畫圖，在使用每一個button之後，如果要再使用其他工具要再按一次，舉例來說，當前的工具是畫筆，按了一次undo之後，要再按一次畫筆工具才能繼續畫圖，所有工具都是這樣操作，在使用Text的功能時，要先點一次你要畫上文字的部分，然後再點一次方框內開始打字，特別提醒不要在畫布的太下方使用text，因為text的方框會擋住下面選取tools的buttons，


### Function description
canvas主要有四個執行function，分別是在mouseout,mouseup,mousedown,mousemove的時候執行
mouseout -> 停止所有動作，並重新初始化各個flag
mouseup -> 跟mouseout一樣
mousedown -> 先存下當前的圖，並且丟進imgStack，然後會在這判斷是否使用text的功能，有的話就可以開始打字
mousemove -> 在這裡會利用mode來判斷現在選取的工具，然後依此來呼叫不同的function

ct()
把現在使用的tools值傳給mode，之後利用mode來辨識並選擇要使用的function

draw()
基本的畫圖功能

erase()
橡皮擦功能，使用canvas.cleaerect()這個function，並且把形狀變成圓形的，跟著鼠標一起移動來擦掉圖畫

drawCircle()、drawFCircle()
畫空心圈與實心圈的功能，使用undo來清除拖曳時產生的圖形，隨著滑鼠位置，拖曳出想要的大小

drawRectangle()、drawFRectangle()
畫中空長方形與實心長方形的功能，使用undo來清除拖曳時產生的圖形，隨著滑鼠位置，拖曳出想要的大小

drawTriangle()
畫中空三角形的功能，使用undo來清除拖曳時產生的圖

drawText()
畫上文字的功能，在此調整字體大小

document.getElementById('redo').addEventListener()
回到下一步，按下去之後會把當前的Image丟進imgStack，可以使用undo()回到當前圖畫

ocument.getElementById('undo').addEventListener()
回到上一步，按下去之後會把當前的Image丟進redoStack，這樣就可以使用redo()找回當前圖畫

document.getElementById('reset').addEventListener()
重製化布，使用canvas.clearrect()這個function來清空畫布

document.getElementById('upload').addEventListener()
上傳圖片，會把圖片調整成畫布的大小

document.getElementById('download').addEventListener()
下載當前圖畫

getDistance()
算出mousedown的那個位置與當前鼠標位置的距離，主要是輔助算出圓的半徑


### Gitlab page link

https://108062207.gitlab.io/AS_01_WebCanvas

### Others (Optional)

    

<style>
table th{
    width: 100%;
}
</style>