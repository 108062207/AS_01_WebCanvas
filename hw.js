//var test = document.getElementById("pen");
const canvas = document.querySelector('#my-canvas');
const textBox = document.getElementById('my-textbox');
const ctx = canvas.getContext('2d'); 
console.log("nmsl");
// Initialize
ctx.strokeStyle = 'black'; 
ctx.lineJoin = 'round'; 
ctx.lineCap = 'round';  
ctx.lineWidth = 1; 



let isDrawing = false;
let drawing = false;
let lastX = 0;
let lastY = 0;
var now_color = 'black';
var mode = 'pen';
var imgStack = [];
var redoStack = [];
var textFlag = false;
var textContent = "";

canvas.addEventListener('mouseup', () => {
    isDrawing = false;
    drawing = false;
    //console.log('mouseup');
    
});

canvas.addEventListener('mouseout', () => isDrawing = false);
canvas.addEventListener('mousedown', (e) => {
    isDrawing = true;
    
    console.log("push in mousedown");
    const imgData = ctx.getImageData(0,0,canvas.width,canvas.height);
    if(mode != 'rectangle' && mode != 'circle' && mode != 'triangle' &&mode != 'text')imgStack.push(imgData);
    
    [lastX, lastY] = [e.offsetX, e.offsetY];
    
    if(mode == 'text'){
        document.getElementById('my-canvas').style.cursor = "url('./img/text.ico'), auto";
        if (textFlag) {
            textContent = textBox.value;
            //console.log(textContent);
            textFlag = false;
            textBox.style['z-index'] = 1;
            textBox.value = "";
            drawText();
        } else if (!textFlag) {
            //console.log('texting');
            imgStack.push(imgData);
            textFlag = true
            textBox.style.left = lastX + 'px';
            textBox.style.top = lastY + 'px';
            console.log(textBox.style.left, textBox.style.top);
            textBox.style['z-index'] = 6;
        }
    }

});

canvas.addEventListener('mousemove', (e) => {
    redoStack = [];
    ctx.strokeStyle = document.getElementById('palette').value;
    ctx.fillStyle = document.getElementById('palette').value;
    ctx.lineWidth = document.getElementById('crange').value;
    switch(mode) {
        case 'pen':
            draw(e);
            document.getElementById('my-canvas').style.cursor = "url('./img/pencil2.ico'), auto";
            console.log(1);
            break;
        case 'erase':
            console.log(2);
            document.getElementById('my-canvas').style.cursor = "url('./img/eraser2.ico'), auto";
            erase(e,ctx.lineWidth);
            break;
        case 'rectangle':
            console.log(3);
            document.getElementById('my-canvas').style.cursor = "url('./img/rectangle.ico'), auto";
            drawRectangle(e);
            break;
        case 'circle':
            console.log(4);
            document.getElementById('my-canvas').style.cursor = "url('./img/circle.ico'), auto";
            drawCircle(e);
            break;
        case 'triangle':
            console.log(5);
            document.getElementById('my-canvas').style.cursor = "url('./img/triangle.ico'), auto";
            drawTriangle(e);
            break;
        case 'line':
            //line();
            break;
        case 'text':
            document.getElementById('my-canvas').style.cursor = "url('./img/text.ico'), auto";
            break;
        case 'frectangle':
            //console.log(3);
            document.getElementById('my-canvas').style.cursor = "url('./img/frectangle.ico'), auto";
            drawFRectangle(e);
            break;
        case 'fcircle':
            //console.log(4);
            document.getElementById('my-canvas').style.cursor = "url('./img/fcircle.ico'), auto";
            drawFCircle(e);
            break;
    }

});

function draw(e) {
    if(!isDrawing) return;

    ctx.beginPath();
    ctx.moveTo(lastX, lastY);
    ctx.lineTo(e.offsetX, e.offsetY);
    ctx.stroke();
    ctx.closePath();

    [lastX, lastY] = [e.offsetX, e.offsetY];
    
}

function erase(e,r){
    if(!isDrawing) return;
    
    [lastX, lastY] = [e.offsetX, e.offsetY];
    //console.log(lastX , " " , lastY);
    //ctx.beginPath();
    //ctx.clearRect(lastX, lastY, 10, 10);
    //ctx.closePath();
    for( var i = 0 ; i < Math.round( Math.PI * ctx.lineWidth ) ; i++ ){
        var angle = ( i / Math.round( Math.PI * ctx.lineWidth )) * 360;
        ctx.beginPath();
        ctx.clearRect( lastX , lastY , Math.sin( angle * ( Math.PI / 180 )) * ctx.lineWidth , Math.cos( angle * ( Math.PI / 180 )) * ctx.lineWidth );
        ctx.closePath();
    }
}

function drawRectangle(e){
    if(!isDrawing) return;

    if(drawing){
       // console.log('delrec');
        if (imgStack.length > 0) {
            const lp = ctx.getImageData(0,0,canvas.width,canvas.height);
            redoStack.push(lp);
            const imgData = imgStack.pop();
            //redoStack.push(imgData);
            ctx.putImageData(imgData, 0, 0);
        }
        
    }

    const imgData = ctx.getImageData(0,0,canvas.width,canvas.height);
    imgStack.push(imgData);
    //console.log("push in rectangle");

    x = parseInt(e.clientX - canvas.offsetLeft);
    y = parseInt(e.clientY - canvas.offsetTop);
    ctx.beginPath();
    ctx.rect(lastX, lastY,e.offsetX-lastX, e.offsetY-lastY);
    //console.log(lastX ,lastY, e.offsetX, e.offsetY);
    //ctx.strokeStyle = 'black';
    ctx.stroke();
    ctx.closePath();

    drawing = true;
}

function drawCircle(e){
    if(!isDrawing)return;

    x = parseInt(e.clientX - canvas.offsetLeft);
    y = parseInt(e.clientY - canvas.offsetTop);
    radius = getDistance(lastX, lastY,x,y);
    
    if(drawing){
        //console.log('delrec');
        if (imgStack.length > 0) {
            const lp = ctx.getImageData(0,0,canvas.width,canvas.height);
            redoStack.push(lp);
            const imgData = imgStack.pop();
            //redoStack.push(imgData);
            ctx.putImageData(imgData, 0, 0);
        }
        
    }

    const imgData = ctx.getImageData(0,0,canvas.width,canvas.height);
    imgStack.push(imgData);
    //console.log("push in rectangle");

    
    ctx.beginPath();
    ctx.arc(lastX, lastY, radius, 0, 2*Math.PI);
    //ctx.rect(lastX, lastY,e.offsetX-lastX, e.offsetY-lastY);
    //console.log(lastX ,lastY, e.offsetX, e.offsetY);
    //ctx.strokeStyle = 'black';
    ctx.stroke();
    ctx.closePath();

    drawing = true;

}

function drawTriangle(e){
    if(!isDrawing)return;

    dist = getDistance(lastX, lastY,e.offsetX,e.offsetY);
    let height = dist * Math.cos(Math.PI / 6);
    if(drawing){
        //console.log('delrec');
        if (imgStack.length > 0) {
            const lp = ctx.getImageData(0,0,canvas.width,canvas.height);
            redoStack.push(lp);
            const imgData = imgStack.pop();
            //redoStack.push(imgData);
            ctx.putImageData(imgData, 0, 0);
        }
        
    }

    const imgData = ctx.getImageData(0,0,canvas.width,canvas.height);
    imgStack.push(imgData);
    //console.log("push in rectangle");

    
    ctx.beginPath();
    ctx.moveTo(lastX,lastY);
    ctx.lineTo(e.offsetX,e.offsetY);
    ctx.lineTo((e.offsetX+lastX)/2 , lastY - height);
    ctx.closePath();
    //ctx.strokeStyle = 'black';
    ctx.stroke();
    

    drawing = true;

}

function drawText(){
    if(!ctx)return;
    else{
        let f = document.getElementById('fonts').value;
        console.log(f);
        ctx.fillStyle = document.getElementById('palette').value;
        let s = document.getElementById('text-size').value + 'px';
        ctx.save();
        ctx.beginPath();
        ctx.font = s + " " + f;
        console.log(ctx.font);
        ctx.fillText(textContent, parseInt(textBox.style.left), parseInt(textBox.style.top));
        ctx.restore();
        ctx.closePath();
    }
}

function getDistance(p1X, p1Y, p2X, p2Y) {
    return Math.sqrt(Math.pow(p1X - p2X, 2) + Math.pow(p1Y - p2Y, 2))
  }

function ct(c){
    mode = c;
}


document.getElementById('undo').addEventListener('click', () => {
    console.log('undo to ' + (imgStack.length-1));
    if (imgStack.length > 0) {
        const lp = ctx.getImageData(0,0,canvas.width,canvas.height);
        redoStack.push(lp);
        const imgData = imgStack.pop();
        //redoStack.push(imgData);
        ctx.putImageData(imgData, 0, 0);
    }
});

document.getElementById('redo').addEventListener('click', () => {
    //console.log('redo to ' + (redoStack.length-1));
    if (redoStack.length > 0) {
        console.log('redo');
        const lp = ctx.getImageData(0,0,canvas.width,canvas.height);
        imgStack.push(lp);
        const imgData = redoStack.pop();
        
        ctx.putImageData(imgData, 0, 0);
    }
});

document.getElementById('reset').addEventListener('click', () => {
    const imgData = ctx.getImageData(0,0,canvas.width,canvas.height);
    imgStack.push(imgData);
    ctx.clearRect(0, 0, canvas.width, canvas.height);
});

document.getElementById('upload').addEventListener('change', ()=> {
    let fr = document.getElementById('upload');
    if (fr.files && fr.files[0]) {
        var FR = new FileReader();
        FR.onload = function (e) {
            var img = new Image();
            img.onload = function () {
                ctx.drawImage(img, 0, 0,img.width,img.height,0,0,canvas.width,canvas.height);//,0,0,800,600);
                console.log('drawimage');
            };
            img.src = e.target.result;
        };
        FR.readAsDataURL(fr.files[0]);
    }
}); 

document.getElementById('download').addEventListener('click', () => {
    const link = document.createElement('a')
    link.href =  canvas.toDataURL();
    link.download = 'mypaint.png';
    link.click();
});

function drawFCircle(e){
    if(!isDrawing)return;

    x = parseInt(e.clientX - canvas.offsetLeft);
    y = parseInt(e.clientY - canvas.offsetTop);
    radius = getDistance(lastX, lastY,x,y);
    
    if(drawing){
        //console.log('delrec');
        if (imgStack.length > 0) {
            const lp = ctx.getImageData(0,0,canvas.width,canvas.height);
            redoStack.push(lp);
            const imgData = imgStack.pop();
            //redoStack.push(imgData);
            ctx.putImageData(imgData, 0, 0);
        }
        
    }

    const imgData = ctx.getImageData(0,0,canvas.width,canvas.height);
    imgStack.push(imgData);
    //console.log("push in rectangle");

    
    ctx.beginPath();
    ctx.arc(lastX, lastY, radius, 0, 2*Math.PI);
    //ctx.rect(lastX, lastY,e.offsetX-lastX, e.offsetY-lastY);
    //console.log(lastX ,lastY, e.offsetX, e.offsetY);
    //ctx.strokeStyle = 'black';
    ctx.fill();
    ctx.closePath();

    drawing = true;

}

function drawFRectangle(e){
    if(!isDrawing) return;

    if(drawing){
       // console.log('delrec');
        if (imgStack.length > 0) {
            const lp = ctx.getImageData(0,0,canvas.width,canvas.height);
            redoStack.push(lp);
            const imgData = imgStack.pop();
            //redoStack.push(imgData);
            ctx.putImageData(imgData, 0, 0);
        }
        
    }

    const imgData = ctx.getImageData(0,0,canvas.width,canvas.height);
    imgStack.push(imgData);
    //console.log("push in rectangle");

    x = parseInt(e.clientX - canvas.offsetLeft);
    y = parseInt(e.clientY - canvas.offsetTop);
    ctx.beginPath();
    ctx.rect(lastX, lastY,e.offsetX-lastX, e.offsetY-lastY);
    //console.log(lastX ,lastY, e.offsetX, e.offsetY);
    //ctx.strokeStyle = 'black';
    ctx.fill();
    ctx.closePath();

    drawing = true;
}

